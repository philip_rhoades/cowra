---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
---

<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="BeefUp is a responsive jQuery accordion plugin">
<meta name="keywords" content="beefup, jquery, accordion, css, javascript, plugin, collapsible">
<link rel="stylesheet" href="dist/css/styles.css">
<link rel="stylesheet" href="dist/css/jquery.beefup.css">
<link href='https://fonts.googleapis.com/css?family=Roboto:300,700' rel='stylesheet'>
</head>

<body>

<h2> About </h2>

<p> 
This is the web site for free listing of Cowra organisations - usually ones that don't have there own website
</p> 

<!--
<h3> Help support this site </h3>
<a href="https://www.patreon.com/bePatron?u=28998545" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>
<br>
<br>
-->

<p><i> 
In the accordion below click on a heading or subheading to expand or collapse a section.
</i></p>

<main>
<!-- Cat0 -->
	<article class="beefup example-opensingle is-open">
		<h4 class="beefup__head">
			{{ site.data.cat00.acc[0].title }}
		</h4>

		<div class="beefup__body">
			<p>
			{{ site.data.cat00.acc[0].description }}
			</p>

			<article class="beefup example-opensingle is-open">
				<h4 class="beefup__head">
					{{ site.data.cat00.sub0.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat00.sub0.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle">
				<h4 class="beefup__head">
					{{ site.data.cat00.sub1.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat00.sub1.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle is-closed">
				<h4 class="beefup__head">
					{{ site.data.cat00.sub2.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat00.sub2.acc[0].description }}
				</div>
			</article>

		</div>
	</article>

<!-- Cat1 -->
	<article class="beefup example-opensingle">
		<h4 class="beefup__head">
			{{ site.data.cat01.acc[0].title }}
		</h4>

		<div class="beefup__body">
			<p>
			{{ site.data.cat01.acc[0].description }}
			</p>

			<article class="beefup example-opensingle is-open">
				<h4 class="beefup__head">
					{{ site.data.cat01.sub0.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat01.sub0.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle">
				<h4 class="beefup__head">
					{{ site.data.cat01.sub1.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat01.sub1.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle is-closed">
				<h4 class="beefup__head">
					{{ site.data.cat01.sub2.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat01.sub2.acc[0].description }}
				</div>
			</article>

		</div>
	</article>

<!-- Cat2 -->
	<article class="beefup example-opensingle">
		<h4 class="beefup__head">
			{{ site.data.cat02.acc[0].title }}
		</h4>

		<div class="beefup__body">
			<p>
			{{ site.data.cat02.acc[0].description }}
			</p>

			<article class="beefup example-opensingle is-open">
				<h4 class="beefup__head">
					{{ site.data.cat02.sub0.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat02.sub0.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle">
				<h4 class="beefup__head">
					{{ site.data.cat02.sub1.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat02.sub1.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle is-closed">
				<h4 class="beefup__head">
					{{ site.data.cat02.sub2.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat02.sub2.acc[0].description }}
				</div>
			</article>

		</div>
	</article>

<!-- Cat3 -->
	<article class="beefup example-opensingle">
		<h4 class="beefup__head">
			{{ site.data.cat03.acc[0].title }}
		</h4>

		<div class="beefup__body">
			<p>
			{{ site.data.cat03.acc[0].description }}
			</p>

			<article class="beefup example-opensingle is-open">
				<h4 class="beefup__head">
					{{ site.data.cat03.sub0.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat03.sub0.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle">
				<h4 class="beefup__head">
					{{ site.data.cat03.sub1.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat03.sub1.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle is-closed">
				<h4 class="beefup__head">
					{{ site.data.cat03.sub2.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat03.sub2.acc[0].description }}
				</div>
			</article>

		</div>
	</article>

<!-- Cat4 -->
	<article class="beefup example-opensingle">
		<h4 class="beefup__head">
			{{ site.data.cat04.acc[0].title }}
		</h4>

		<div class="beefup__body">
			<p>
			{{ site.data.cat04.acc[0].description }}
			</p>

			<article class="beefup example-opensingle is-open">
				<h4 class="beefup__head">
					{{ site.data.cat04.sub0.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat04.sub0.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle">
				<h4 class="beefup__head">
					{{ site.data.cat04.sub1.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat04.sub1.acc[0].description }}
				</div>
			</article>

			<article class="beefup example-opensingle is-closed">
				<h4 class="beefup__head">
					{{ site.data.cat04.sub2.acc[0].title }}
				</h4>

				<div class="beefup__body">
					{{ site.data.cat04.sub2.acc[0].description }}
				</div>
			</article>

		</div>
	</article>

</main>

<script src="dist/js/jquery.min.js"></script>
<script src="dist/js/jquery.beefup.min.js"></script>
<script>
	$(function() {

		// Default
		$('.example').beefup();

		// Open single
		$('.example-opensingle').beefup({
			openSingle: true,
			stayOpen: 'last'
		});

		// Fade animation
		$('.example-fade').beefup({
			animation: 'fade',
			openSpeed: 400,
			closeSpeed: 400
		});

		// Scroll
		$('.example-scroll').beefup({
			scroll: true,
			scrollOffset: -10
		});

		// Self block
		$('.example-selfblock').beefup({
			selfBlock: true
		});

		// Self close
		$('.example-selfclose').beefup({
			selfClose: true
		});

		// Breakpoints
		$('.example-breakpoints').beefup({
			scroll: true,
			scrollOffset: -10,
			breakpoints: [
				{
					breakpoint: 768,
					settings: {
						animation: 'fade',
						scroll: false
					}
				},
				{
					breakpoint: 1024,
					settings: {
						animation: 'slide',
						openSpeed: 800,
						openSingle: true
					}
				}
			]
		});

		// API Methods
		var $beefup = $('.example-api').beefup();
		$beefup.open($('#beefup'));

		// Callback
		$('.example-callbacks').beefup({
			onInit: function($el) {
				$el.css('border-color', 'blue');
			},
			onOpen: function($el) {
				$el.css('border-color', 'green');
			},
			onClose: function($el) {
				$el.css('border-color', 'red');
			}
		});

		// Use HTML5 data attributes
		$('.example-data').beefup();

		// Tabs
		$('.tab__item').beefup({
			animation: '',
			openSingle: true,
			openSpeed: 0,
			closeSpeed: 0,
			onOpen: function($el) {
				// Add active class to tabs
				$('a[href="#' + $el.attr('id') + '"]').parent().addClass(this.openClass)
						.siblings().removeClass(this.openClass);
			}
		});

		// Dropdown
		var $dropdown = $('.dropdown').beefup({
			animation: 'fade',
			openSingle: true,
			selfClose: true
		});

		// Close dropdown
		$('.dropdown').on('click', 'li', function() {
			$dropdown.close();
		});
	});
</script>

<h2> Posts </h2>

<!--
<h3> Help this site </h3>
<a href="https://www.patreon.com/bePatron?u=28998545" data-patreon-widget-type="become-patron-button">Become a Patron!</a><script async src="https://c6.patreon.com/becomePatronButton.bundle.js"></script>
<br>
<br>
-->

</body>



